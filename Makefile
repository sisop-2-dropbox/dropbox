CC=gcc

all: clean client server

client: dropboxClient.o dropboxUtil.o lista.o
	$(CC) -g -o client dropboxClient.o dropboxUtil.o lista.o -Wall -lpthread

server: dropboxServer.o dropboxUtil.o lista.o
	$(CC) -g -o server dropboxServer.o dropboxUtil.o lista.o -Wall -lpthread

dropboxClient.o: dropboxClient.c dropboxClient.h
	$(CC) -g -c dropboxClient.c -Wall

dropboxServer.o: dropboxServer.c dropboxServer.h
	$(CC) -g -c dropboxServer.c -Wall

dropboxUtil.o: dropboxUtil.c dropboxUtil.h
	$(CC) -g -c dropboxUtil.c -Wall

lista.o: lista.c lista.h
	$(CC) -g -c lista.c -Wall

clean:
	rm -rf client server *.o  *~ *.dSYM