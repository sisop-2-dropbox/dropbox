#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <libgen.h>
#include "dropboxUtil.h"
#include "dropboxClient.h"
#include <pthread.h>

int client_fd = 0;
int daemonRunning = 0;
pthread_t daemonTid = -1;
char userid[MAXNAME];
char * home_path;

/***********************************************************************
 * Sincroniza o diretório “sync_dir_<nomeusuário>” com o servidor.
 * ********************************************************************/
void sync_client()
{

}


/***********************************************************************
 * Envia um arquivo file para o servidor.
 * Deverá ser executada quando for realizar upload de um arquivo.
 * file -filename.ext do arquivo a ser enviado
 * ********************************************************************/
void send_file(char *file)
{
	printf("Sending file %s...\n", file);

	FILE *handler = fopen(file, "r");
	char buffer[BLOCK_SIZE];
	ssize_t r;

	if (handler == NULL){
		sprintf(buffer, "Could not find specified file, aborting.");
		write(client_fd, buffer, BLOCK_SIZE);
		printf("%s\n", buffer);
	}
	else{
		sprintf(buffer, "rdy");
		write(client_fd, buffer, BLOCK_SIZE);

		read(client_fd, buffer, BLOCK_SIZE);

		if (strcmp(buffer, "ack") == 0){
			printf("Ready to send data...\n");
			while (!feof(handler)){
				r = fread(buffer, sizeof(char), BLOCK_SIZE, handler);
				write(client_fd, buffer, r);
			}

			buffer[0] = '\0';
			write(client_fd, buffer, 1);
			printf("File sent!\n");
		}else{
			printf("Error: %s\n", buffer);
		}

		fclose(handler);
	}

}


/***********************************************************************
 * Obtém um arquivo file do servidor.
 * Deverá ser executada quando for realizar download de um arquivo.
 * file -filename.ext
 * ********************************************************************/
void get_file(char *file)
{

}


/**
 *
 **/
int delete_from_server(char *file)
{
	char buffer[256];

	bzero(buffer, 256);
	sprintf(buffer, "delete_file %s", file);

	if (write(client_fd, buffer, 256) > 0)
	{
		bzero(buffer, 256);

		if (read(client_fd, buffer, 256) > 0)
		{
			if (strcmp(buffer, "done") == 0)
			{
				return 1;
			}
		}
	}

	return 0;
}



/***********************************************************************
 * Conecta o cliente com o servidor.
 * host - endereço do servidor
 * port - port aguardando conexão
 * ********************************************************************/
void connect_server(char * host, int port)
{
	int bytes;
	char buffer[256];
	struct sockaddr_in serv_addr;
	struct hostent *server;

	server = gethostbyname(host);

	if (server == NULL)
	{
		 printf("No such host\n");
	}
	else
	{
		if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		{
			printf("Error opening socket\n");
		}
		else
		{
			serv_addr.sin_family = AF_INET;
			serv_addr.sin_port = htons(port);
			serv_addr.sin_addr = *((struct in_addr *)server->h_addr);
			bzero(&(serv_addr.sin_zero), 8);

			if (connect(client_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
			{
				printf("Error connecting\n");
			}
			else
			{
				bzero(buffer, 256);
				strncpy(buffer, userid, 256);
				bytes = write(client_fd, buffer, 256);

				if (bytes < 0)
				{
					printf("Error writing to socket\n");
				}
				else
				{
					bzero(buffer, 256);
					bytes = read(client_fd, buffer, 256);

					if (bytes < 0)
					{
						printf("Error reading from socket\n");
					}
					else
					{
						printf("%s\n", buffer);

						if (strstr(buffer, "Welcome"))
						{
							get_sync_dir();
							client_commands();
						}
					}
				}
			}
		}
	}
}



/***********************************************************************
 *
 * ********************************************************************/
void client_commands()
{
	char * token;
	char buffer[256], cmdline[256];

	while(1)
	{
		bzero(buffer, 256);
		bzero(cmdline, 256);

		printf("command> ");
		fgets(buffer, 256, stdin);
		strtok(buffer, "\n");
		strtok(buffer, "\r");

		memmove(cmdline, buffer, strlen(buffer));
		token = strtok(cmdline, " ");

		/**
		 * Upload command
		 **/
		if (strcmp(token, "upload") == 0)
		{
			char * buf;
			int size;
			time_t t;

			token = strtok(NULL, "\n");

			if (token)
			{
				buf = read_file(token, &size);

				if (buf)
				{
					if (write(client_fd, buffer, 256) < 0)
					{
						printf("Error sending the command\n");
					}
					else
					{
						send_data(client_fd, buf, size);

						t = get_mtime(token);

						if (t)
						{
							change_mtime(client_fd, token, t);
						}
					}

					free(buf);
				}
				else
				{
					printf("Could not open the file\n");
				}
			}
			else
			{
				printf("File not specified\n");
			}
		}

		/**
		 * Download command
		 **/
		else if (strcmp(token, "download") == 0)
		{
			int size;
			char * buf, * filename;
			time_t t;

			token = strtok(NULL, "\n");

			if (token)
			{
				if (write(client_fd, buffer, 256) < 0)
				{
					printf("Error sending the command\n");
				}
				else
				{
					buf = receive_data(client_fd, &size);

					if (buf)
					{
						filename = basename(token);

						write_file(filename, buf, size);
						t = request_mtime(client_fd, filename);

						if (t)
						{
							set_mtime(filename, t);
						}

						free(buf);
					}
					else
					{
						printf("Download failed\n");
					}
				}
			}
			else
			{
				printf("File not specified\n");
			}
		}

		/**
		 * List server command
		 **/
		else if (strcmp(token, "list_server") == 0)
		{
			int size;
			char * buf;

			if (write(client_fd, buffer, 256) < 0)
			{
				printf("Error sending the command\n");
			}
			else
			{
				buf = receive_data(client_fd, &size);

				if (buf)
				{
					printf("%s\n", buf);
					free(buf);
				}
				else
				{
					printf("List server failed\n");
				}
			}
		}

		/**
		 * Get file list command
		 **/
		else if (strcmp(token, "get_file_list") == 0)
		{
			char * buf;

			buf = get_server_file_list();

			if (buf)
			{
				printf("%s\n", buf);
				free(buf);
			}
			else
			{
				printf("Error listing files");
			}
		}

		/**
		 * List client command
		 **/
		else if (strcmp(token, "list_client") == 0)
		{
			char * buf;
			char homedir[256];

			bzero(homedir, 256);
			sprintf(homedir, "%s/sync_dir_%s", home_path, userid);
			buf = list_directory(homedir);

			if (buf)
			{
				printf("%s\n", buf);
				free(buf);
			}
			else
			{
				printf("List client failed\n");
			}
		}

		/**
		 * Remove file command
		 **/
		else if (strcmp(token, "remove") == 0)
		{
			char homedir[256];

			token = strtok(NULL, "\n");

			if (token)
			{
				bzero(homedir, 256);
				sprintf(homedir, "%s/sync_dir_%s/%s", home_path, userid, basename(token));

				delete_file(homedir);
				delete_from_server(basename(token));

				printf("Done\n");
			}
			else
			{
				printf("File not specified\n");
			}
		}

		/**
		 * Get sync dir command
		 **/
		else if (strcmp(token, "get_sync_dir") == 0)
		{
			get_sync_dir();
		}

		/**
		 * Help command
		 **/
		else if (strcmp(token, "help") == 0)
		{
			printf("Commands\n"
					"\tupload filename\n"
					"\tdownload filename\n"
					"\tlist_server\n"
					"\tlist_client\n"
					"\tget_sync_dir\n"
					"\tremove filename\n"
					"\thelp\n"
					"\texit\n");
		}

		/**
		 * Exit command
		 **/
		else if (strcmp(token, "exit") == 0)
		{
			notify_disconnect();
			break;
		}

		/**
		 * Invalid command
		 **/
		else
		{
			printf("Invalid command\n");
		}
	}
}


/***********************************************************************
 * Fecha a conexão com o servidor
 * ********************************************************************/
void close_connection()
{
	if (client_fd)
	{
		close(client_fd);
		client_fd = 0;
	}
}


/***********************************************************************
 * Notifica o servidor que o cliente vai fechar a conexão
 * ********************************************************************/
void notify_disconnect()
{
	char output[256];

	if (client_fd)
	{
		bzero(output, 256);
		sprintf(output, "exit");
		write(client_fd, output, 256);
	}
}


/***********************************************************************
 * Captura sinal de SIGINT (ctrl+c) enviado ao processo
 * ********************************************************************/
void intHandler(int s)
{
	notify_disconnect();
	close_connection();
	exit(SUCCESS);
}


/**
 *
 **/

void * daemonSync(void * arg){
	char *clientFile[MAXFILES];
	char *serverFile[MAXFILES];

	int clientNum = 0;
	int serverNum = 0;

	int client_file;
	int server_file;

	char *token;

	char dirPath[MAXPATH];

	sprintf(dirPath, "%s/sync_dir_%s", home_path, userid);



	char *clientList = get_file_list(dirPath); //FIX PARAM
	char *serverList = get_server_file_list();


	int size;
	char buffer[256];
	char * buf, *filename;
	time_t t;

	while(1){
		printf("Syncing...\n");

		clientNum = 0;
		serverNum = 0;

		token = strtok(clientList, "\n");
		while (token){
			clientFile[clientNum] = token;
			clientNum++;

			token = strtok(NULL, "\n");
		}

		token = strtok(serverList, "\n");
		while (token){
			serverFile[serverNum] = token;
			serverNum++;

			token = strtok(NULL, "\n");
		}

		for (server_file = 0; server_file < serverNum; server_file++){
			int found = 0;
			for (client_file = 0; client_file < clientNum; client_file++){
			//IF FOUND
				if (strcmp(clientFile[client_file], serverFile[server_file]) == 0){
				//compare versions

					time_t localTime = get_mtime(clientFile[client_file]);
					time_t serverTime = request_mtime(client_fd, serverFile[server_file]);

					double diffTime = difftime(serverTime, localTime);

					if (diffTime > 0){
					//server file is updated, client is not

						buf = receive_data(client_fd, &size);

						if (buf)
						{
							filename = basename(serverFile[server_file]);

							write_file(filename, buf, size);

							t = serverTime;

							if (t)
							{
								set_mtime(filename, t);
							}

							free(buf);
						}
						else
						{
							printf("Download failed\n");
						}

					}else{
						if (diffTime < 0){
						//client file is updated, server is not
							buf = read_file(clientFile[client_file], &size);

							if (buf)
							{
								if (write(client_fd, buffer, 256) < 0)
								{
									printf("Error sending the command\n");
								}
								else
								{
									send_data(client_fd, buf, size);

									t = localTime;

									if (t)
									{
										change_mtime(client_fd, token, t);
									}
								}

								free(buf);
							}
							else
							{
								printf("Could not open the file\n");
							}
						}
					}

					found = 1;
					break;
				}

			}

			if (!found){
				buf = receive_data(client_fd, &size);

				if (buf)
				{
					filename = basename(serverFile[server_file]);

					write_file(filename, buf, size);

					t = request_mtime(client_fd, serverFile[server_file]);

					if (t)
					{
						set_mtime(filename, t);
					}

					free(buf);
				}
				else
				{
					printf("Download failed\n");
				}
			}
		}
		sleep(10);
	}
	return 0;

}

void get_sync_dir()
{
	char homedir[256];
	int i;

	home_path = get_home();

	bzero(homedir, 256);
	sprintf(homedir, "%s/sync_dir_%s", home_path, userid);

	i = open_directory(homedir);

	/* sync_dir was already present, we should
	 fetch differences and upload or delete files from server
	 that are not present anymore */
	if (i == 1)
	{

	}

	/* sync_dir was created, we should just download
	 the files from server */
	else if (i == 2)
	{

	}

	else
	{
		printf("Failed to open homedir\n");
		exit(ERROR);
	}

	// Create new thread for daemon
	//if (!daemonRunning){
	if (0){
		pthread_create(&daemonTid, NULL, daemonSync, NULL);
		daemonRunning = 1;
	}
}

/**
 * Obtém a listagem de arquivos do servidor separado por \n
 **/
char * get_server_file_list()
{
	int size;
	char * buf;
	char buffer[256];

	bzero(buffer, 256);
	sprintf(buffer, "%s", "get_file_list");

	if (write(client_fd, buffer, 256) < 0)
	{
		printf("Error sending the command\n");
	}
	else
	{
		buf = receive_data(client_fd, &size);

		if (buf)
		{
			if (strcmp(buf, "error") == 0)
			{
				return NULL;
			}
			else
			{
				return buf;
			}
		}
		else
		{
			printf("Get file list failed\n");
		}
	}

	return NULL;
}


/***********************************************************************
 * Função 'main()' do Cliente
 * ********************************************************************/
int main(int argc, char **argv)
{

	/* SIGINT handler (ctrl+c) */
	struct sigaction act_sigint;
	act_sigint.sa_handler = intHandler;
	sigaction(SIGINT, &act_sigint, NULL);

	/* SIGTERM handler */
	struct sigaction act_sigterm;
	act_sigterm.sa_handler = intHandler;
	sigaction(SIGTERM, &act_sigterm, NULL);

	if (argc != 4)
	{
		printf("Usage: %s user host port\n", argv[0]);
		exit(ERROR);
	}

	strcpy(userid, argv[1]);
	connect_server(argv[2], atoi(argv[3]));
	close_connection();

	return SUCCESS;
}
