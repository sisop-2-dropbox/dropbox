/**
 * Uma lista que armazena um ponteiro para qualquer tipo de dado (void *)
 * Obs:
 *	As fun��es que removem elementos da lista, v�o fazer free no N�.
 *	No entanto, N�->dado tem que ser feito free manualmente
 *	Ver arquivo testeLista.c para exemplos de uso
 **/

#ifndef _LISTA_H_
#define _LISTA_H_

typedef struct sNo
{
	void * dado;
	struct sNo * prox;
} NO;

typedef NO* LISTA;

void listaInicializar(LISTA * lista);
void listaListar(LISTA * lista);
int listaTamanho(LISTA *lista);
void * listaPegarEm(LISTA * lista, int posicao);
void listaInserirEm(LISTA * lista, int posicao, void * dado);
void listaRemoverEm(LISTA * lista, int posicao);

#define LINIT(l) listaInicializar(l)
#define LPRINT(l) listaListar(l)
#define LSIZE(l) listaTamanho(l)
#define LGET(l, p) listaPegarEm(l, p)
#define LPUT(l, p, d) listaInserirEm(l, p, d)
#define LAPPEND(l, d) listaInserirEm(l, -1, d)
#define LDEL(l, p) listaRemoverEm(l, p)

#endif