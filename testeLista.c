#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

typedef struct sDATA {
	int dia;
	int mes;
	int ano;
} DATA;

// Como compilar?
// gcc -o testeLista testeLista.c lista.c

int main()
{
	LISTA l;
	LINIT(&l);
	int i;
	int a = 10;
	int b = 11;
	int c = 12;
	int d = 13;
	int * e;
	DATA dt = { 24, 10, 2017 };
	DATA *dtPtr, *dtPtr2;

	printf("Testando inserir: -1\n");
	LPUT(&l, -1, &a);
	LPRINT(&l);
	printf("--\n");

	printf("Testando inserir: 0\n");
	LPUT(&l, 0, &a);
	LPRINT(&l);
	printf("--\n");

	printf("Testando inserir: 1\n");
	LPUT(&l, 1, &b);
	LPRINT(&l);
	printf("--\n");

	printf("Testando inserir: 1\n");
 	LPUT(&l, 1, &c);
	LPRINT(&l);
	printf("--\n");

	printf("Testando inserir: 2\n");
 	LPUT(&l, 2, &d);
	LPRINT(&l);
	printf("--\n");

	printf("Testando inserir: -1\n");
	LPUT(&l, -1, &a);
	LPRINT(&l);
	printf("--\n");

	printf("Testando pegar\n");
	e = LGET(&l, -1);
	if (e)
		printf("Dado Pego -1: %d\n", *e);

	e = LGET(&l, 0);
	if (e)
		printf("Dado Pego 0: %d\n", *e);

	e = LGET(&l, 1);
	if (e)
		printf("Dado Pego 1: %d\n", *e);

	e = LGET(&l, 2);
	if (e)
		printf("Dado Pego 2: %d\n", *e);

	e = LGET(&l, 3);
	if (e)
		printf("Dado Pego 3: %d\n", *e);

	e = LGET(&l, 4);
	if (e)
		printf("Dado Pego 4: %d\n", *e);

	e = LGET(&l, 5);
	if (e)
		printf("Dado Pego 5: %d\n", *e);

	printf("Testando remover: -1\n");
	LDEL(&l, -1);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 0\n");
	LDEL(&l, 0);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 1\n");
	LDEL(&l, 1);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 2\n");
	LDEL(&l, 2);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 3\n");
	LDEL(&l, 3);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 4\n");
	LDEL(&l, 4);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 0\n");
	LDEL(&l, 0);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 0\n");
	LDEL(&l, 0);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 0\n");
	LDEL(&l, 0);
	LPRINT(&l);
	printf("--\n");

	printf("Testando remover: 0\n");
	LDEL(&l, 0);
	LPRINT(&l);
	printf("--\n");

	printf("Testando inserir: -1\n");
	LPUT(&l, -1, &a);
	LPRINT(&l);
	printf("--\n");

	printf("Testando inserir: 0\n");
	LPUT(&l, 0, &a);
	LPRINT(&l);
	printf("--\n");



 	printf("Armazenando uma estrutura na lista\n");
 	LPUT(&l, 0, &dt);
 	dtPtr = LGET(&l, 0);
 	if (dtPtr)
 		printf("D:%d, M:%d, A:%d\n", dtPtr->dia, dtPtr->mes, dtPtr->ano);


 	printf("Alocando memoria, inserindo na lista, pegando da lista e fazendo free\n");
 	dtPtr = calloc(1, sizeof(DATA));
 	(*dtPtr).dia = 10;
 	(*dtPtr).mes = 11;
 	(*dtPtr).ano = 2012;

 	LPUT(&l, 0, dtPtr);
 	dtPtr2 = LGET(&l, 0);
 	if (dtPtr2)
 	{
	 	printf("D:%d, M:%d, A:%d\n", dtPtr2->dia, dtPtr2->mes, dtPtr2->ano);

	 	//Fazendo free no n� da lista
	 	// Note que antes de fazer free nisto � preciso fazer free em e->dado caso
	 	// tamb�m tenha sido alocado memoria para tal, como abaixo:

	 	// Fazendo free na memoria alocada que foi colocada na lista
		free(dtPtr2);

		LDEL(&l, 0);
	}

 	printf("Aqui vai a listagem mais uma vez. Note que o primeiro elemento vai sair cortado pois eh uma struct com a data 24/10/2017 e a func de print so imprime inteiros.\n");
 	LPRINT(&l);

 	printf("Lista de tamanho %d\n", LSIZE(&l));

	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);
	LDEL(&l, 0);

	printf("A lista agora deve estar vazia:\n");
	LPRINT(&l);

 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);
 	LPUT(&l, 0, &dt);

 	printf("Agora preenchida com: %d\n", LSIZE(&l));


	printf("Fazendo iteracoes na lista\n");

	DATA * dtPtr3;
	for (i = 0; i < LSIZE(&l); i++)
	{
		dtPtr3 = LGET(&l, i);
		if (dtPtr3)
	 		printf("D:%d, M:%d, A:%d\n", dtPtr3->dia, dtPtr3->mes, dtPtr3->ano);
	}

	return 0;
}