#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <utime.h>
#include <libgen.h>
#include <pwd.h>
#include "dropboxUtil.h"


/**
 * Abre o diretório dirname
 * Caso o mesmo não exista, será criado
 * Diretório ja existente, retorna 1, recém criado, retorna 2
 **/
int open_directory(char * dirname)
{
	DIR* dir = opendir(dirname);

	if (dir)
	{
		closedir(dir);
		return 1;
	}
	else if (ENOENT == errno)
	{
		if (mkdir(dirname, 0700) == 0)
			return 2;
	}
	return 0;
}


/**
 * Obtem a listagem dos arquivos de um diretório, cada arquivo separado por \n
 **/
char * get_file_list(char * path)
{
	DIR *dp;
	struct dirent *ep;
	char * output, * buf, * buf2;
	int i;

	dp = opendir(path);

	if (dp)
	{
		i = 0;
		output = NULL;

		while ((ep = readdir(dp)))
		{
			if (strcmp(ep->d_name, ".") == 0)
				continue;

			if (strcmp(ep->d_name, "..") == 0)
				continue;

			if (i == 0)
			{
				output = calloc(strlen(ep->d_name) + 1, sizeof(char));
				strcpy(output, ep->d_name);
			}
			else
			{
				buf = calloc(strlen(ep->d_name) + 1, sizeof(char));
				strcpy(buf, ep->d_name);

				buf2 = calloc(strlen(output) + strlen(buf) + 2, sizeof(char));
				sprintf(buf2, "%s\n%s", output, buf);

				free(buf);
				free(output);

				output = buf2;
			}
			i++;
		}

		closedir(dp);

		return output;
	}

	return NULL;
}


/**
 * Obtem a listagem dos arquivos de um diretório,
 * aloca e retorna a string correspondente
 * às informações do diretório ou NULL caso haja algum erro
 **/
char * list_directory(char * path)
{
	DIR *dp;
	struct dirent *ep;
	char * output, * buf, * buf2;
	char filename[256];

	dp = opendir(path);

	if (dp)
	{
		output = calloc(101, sizeof(char));

		if (output)
		{
			sprintf(output,"%-24s%-10s%-22s%-22s%-22s",
				"Name",
				"Size",
				"Creation",
				"Access",
				"Modification"
			);

			while ((ep = readdir(dp)))
			{
				if (strcmp(ep->d_name, ".") == 0)
					continue;

				if (strcmp(ep->d_name, "..") == 0)
					continue;

				buf = NULL;
				buf2 = NULL;
				bzero(filename, 256);
				sprintf(filename, "%s/%s", path, ep->d_name);
				buf = list_file(filename);

				if (buf)
				{
					buf2 = calloc(strlen(buf) + strlen(output) + 2, sizeof(char));

					if (buf2)
					{
						sprintf(buf2, "%s\n%s", output, buf);
						free(output);
						output = buf2;
					}

					free(buf);
				}
			}

			closedir(dp);

			return output;
		}
	}

	return NULL;
}


/**
 * Informações sobre um arquivo, aloca e retorna a string correspondente
 * às informações do arquivo ou NULL caso haja algum erro
 **/
char * list_file(char * path)
{
	struct stat sb;
	char * name, * output;
	char buffer[256];
	char fctime[32], fatime[32], fmtime[32];

	if (stat(path, &sb) != -1)
	{
		name = basename(path);
		strftime(fctime, 256, "%Y-%m-%d %H:%M:%S", localtime(&sb.st_ctime));
		strftime(fatime, 256, "%Y-%m-%d %H:%M:%S", localtime(&sb.st_atime));
		strftime(fmtime, 256, "%Y-%m-%d %H:%M:%S", localtime(&sb.st_mtime));

		sprintf(buffer,"%-24s%-10lld%-22s%-22s%-22s",
			name,
			(long long) sb.st_size,
			fctime,
			fatime,
			fmtime
		);

		output = calloc(strlen(buffer) + 1, sizeof(char));

		if (output)
		{
			strcpy(output, buffer);
			return output;
		}
	}

	return NULL;
}

/***********************************************************************
 * Recebe um fluxo de dados de tamanho size (retornado por referencia)
 * ********************************************************************/
char * receive_data(int fd, int * size)
{
	int offset, len;
	char * output;
	char buffer[256];

	bzero(buffer, 256);

	if (read(fd, buffer, 14) < 0)
	{
		printf("Error reading command from socket from device %d\n", fd);
	}
	else
	{
		if (strcmp(buffer, "begin transfer") == 0)
		{
			bzero(buffer, 256);

			if (read(fd, buffer, 11) < 0)
			{
				printf("Error reading size from socket from device %d\n", fd);
			}
			else
			{
				*size = atoi(buffer);
				output = calloc(*size, sizeof(char));

				if (output)
				{
					offset = 0;

					while(offset < *size)
					{
						len = (*size - offset >= 256 ? 256 : *size - offset);
						bzero(buffer, 256);

						if (read(fd, buffer, len) < 0)
						{
							printf("Data transfer failed at offset %d from device %d\n", offset, fd);
							break;
						}

						memmove(output + offset, buffer, len);
						offset += len;
					}

					return output;
				}
			}
		}
		else
		{
			printf("Expected 'begin transfer' got '%s' from device %d\n", buffer, fd);
		}
	}

	return NULL;
}


/***********************************************************************
 * data: dados a serem enviados
 * size: tamanho dos dados a serem enviados
 * fd: socket de destino
 * ********************************************************************/
void send_data(int fd, char * data, int size)
{
	char buffer[256];
	int offset, len;

	bzero(buffer, 256);
	sprintf(buffer, "%s", "begin transfer");

	if (write(fd, buffer, 14) != -1)
	{
		bzero(buffer, 256);
		sprintf(buffer,"%d", size);

		if (write(fd, buffer, 11) != -1)
		{
			offset = 0;

			while(offset < size)
			{
				len = (size - offset >= 256 ? 256 : size - offset);
				bzero(buffer, 256);
				memmove(buffer, data+offset, len);

				if (write(fd, buffer, len) < 0)
				{
					printf("Data transfer failed at offset %d to device %d\n", offset, fd);
					break;
				}

				offset += len;
			}
		}
		else
		{
			printf("Fail to write data size of transfer to device %d\n", fd);
		}
	}
	else
	{
		printf("Fail to write begin of transfer to device %d\n", fd);
	}
}


/**
 * Faz a leitura de um arquivo para a memória e retorna
 * O tamanho do arquivo é salvo por referência em size
 **/
char * read_file(char * path, int * size)
{
	FILE * f;
	char * output = NULL;

	f = fopen(path, "rb");

	if (f)
	{
		fseek(f, 0, SEEK_END);
		*size = ftell(f);
		fseek(f, 0, SEEK_SET);
		output = calloc(*size + 1, sizeof(char));

		if (output)
		{
			fread(output, *size, 1, f);
		}

		fclose(f);
	}

	return output;
}


/**
 * Escreve em um arquivo. Caso o arquivo não exista, o mesmo é criado
 **/
void write_file(char * path, char * data, int size)
{
	FILE *f;

	f = fopen(path, "w");
	if (f)
	{
		fwrite(data, size, 1, f);
		fclose(f);
	}
}


/**
 * Obtém o caminho para a pasta home do usuario
 **/
char * get_home()
{
	char *homedir;

	if ((homedir = getenv("HOME")) == NULL)
	{
		homedir = getpwuid(getuid())->pw_dir;
	}

	return homedir;
}


/**
 * Retorna o modification time de um arquivo
 **/
time_t get_mtime(char * path)
{
	struct stat sb;

	if (stat(path, &sb) != -1)
	{
		return sb.st_mtime;
	}

	return 0;
}


/**
 * Define o modification time de um arquivo
 **/
int set_mtime(char * path, time_t t)
{
	struct stat sb;
	struct utimbuf new_times;

	if (stat(path, &sb) != -1)
	{
		new_times.actime = sb.st_atime;
		new_times.modtime = t;

		if (utime(path, &new_times) == 0)
		{
			return 1;
		}
	}

	return 0;
}


/**
 * Retorna o modification time de um arquivo
 **/
time_t request_mtime(int fd, char * filename)
{
	char buffer[256];
	time_t t;

	bzero(buffer, 256);
	sprintf(buffer, "%s", "request_mtime");

	if (write(fd, buffer, 256) > 0)
	{
		bzero(buffer, 256);
		sprintf(buffer,"%s", filename);

		if (write(fd, buffer, 256) > 0)
		{
			bzero(buffer, 256);

			if (read(fd, buffer, 256) > 0)
			{
				if (strcmp(buffer, "error") == 0)
				{
					printf("Error in the mtime response\n");
				}
				else
				{
					t = atoi(buffer);
					return t;
				}
			}
			else
			{
				printf("Fail to read the mtime request response\n");
			}
		}
		else
		{
			printf("Fail to write filename in the mtime request\n");
		}
	}
	else
	{
		printf("Fail to write begin of mtime request\n");
	}

	return 0;
}


/**
 * Define o modification time de um arquivo
 **/
int change_mtime(int fd, char * filename, time_t t)
{
	char buffer[256];

	bzero(buffer, 256);
	sprintf(buffer, "%s", "change_mtime");

	if (write(fd, buffer, 256) > 0)
	{
		bzero(buffer, 256);
		sprintf(buffer,"%s", filename);

		if (write(fd, buffer, 256) > 0)
		{
			bzero(buffer, 256);
			sprintf(buffer,"%ld", t);

			if (write(fd, buffer, 256) > 0)
			{
				bzero(buffer, 256);

				if (read(fd, buffer, 256) > 0)
				{
					if (strcmp(buffer, "error") == 0)
					{
						printf("Error in the mtime change response\n");
					}
					else
					{
						return 1;
					}
				}
				else
				{
					printf("Fail to read the mtime request response\n");
				}
			}
			else
			{
				printf("Fail to write time in the mtime change request\n");
			}
		}
		else
		{
			printf("Fail to write filename in the mtime change request\n");
		}
	}
	else
	{
		printf("Fail to write begin of mtime change request\n");
	}

	return 0;
}


/**
 * Apaga um arquivo do disco
 **/
int delete_file(char *file)
{
	if (remove(file) == 0)
	{
		return 1;
	}

	return 0;
}
