#ifndef _DROPBOXCLIENT_H_
#define _DROPBOXCLIENT_H_

void connect_server(char * host, int port);
void sync_client();
void send_file(char *file);
void get_file(char *file);
void close_connection();
char * get_server_file_list();
void client_commands();
void notify_disconnect();
void intHandler(int s);
void get_sync_dir();

#endif
