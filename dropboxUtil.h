#ifndef _DROPBOXUTIL_H_
#define _DROPBOXUTIL_H_

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

/// Constantes do programa
#define SUCCESS 0    /// função terminou com sucesso
#define ERROR 1      /// função executou incorretamente
#define FOUND 1      /// função encontrou um elemento
#define NOT_FOUND -1 /// função não encontrou um elemento
#define FAILURE -1   /// o sistema entrou em "pane", em uma situação fatal
#define INVALID -1   /// informação inválida

#define BLOCK_SIZE 1250

/// Constantes das estruturas
#define MAXDEVICES 2      /// quant. máx. de dispositivos que um cliente pode conectar ao servidor
#define MAXNAME 65        /// incluindo o '\0'
#define MAXFILES 32       /// quant. máx. de arquivos por cliente no servidor
#define MAXCLIENTS 32     /// quant. máx. de clientes conectados ao servidor (?)
#define MAXPATH 255       /// Limite de tamanho de um caminho + nome de arquivo

/***********************************************************************
 * name[MAXNAME] refere-se ao nome do arquivo.
 * extension[MAXNAME] refere-se ao tipo de extensão do arquivo.
 * last_modified [MAXNAME] refere-se a data da última modificação no arquivo.
 * size indica o tamanho do arquivo, em bytes.
 * ********************************************************************/
struct file_info {
    char name[MAXNAME];
    char extension[MAXNAME];
    char last_modified[MAXNAME];
    int	size;
};

/***********************************************************************
 * devices[2] – associado aos dispositivos do usuário.
 * userid[MAXNAME] – id do usuário no servidor, que deverá ser único.
 *                   Informado pela linha de comando.
 * file_info[MAXFILES] – metadados de cada arquivo que o cliente possui no servidor.
 * logged_in – cliente está logado ou não.
 * ********************************************************************/
struct client {
    int devices[MAXDEVICES];
    char userid[MAXNAME];
    struct file_info file_info[MAXFILES];
    int logged_in;
};

int open_directory(char * dirname);
char * get_file_list(char * path);
char * list_directory(char * path);
char * list_file(char * path);
char * receive_data(int fd, int * size);
void send_data(int fd, char * data, int size);
char * read_file(char * path, int * size);
void write_file(char * path, char * data, int size);
char * get_home();
time_t get_mtime(char * path);
int set_mtime(char * path, time_t t);
time_t request_mtime(int fd, char * filename);
int change_mtime(int fd, char * filename, time_t t);
int delete_file(char *file);
#endif