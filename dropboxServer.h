#ifndef _DROPBOXSERVER_H_
#define _DROPBOXSERVER_H_

#include "lista.h"

#define PORT 8080
#define SERVER_PATH "DropboxOS"

void sync_server();
void receive_file(char *file, int fd);
void send_file(char *file, int fd);
void intHandler(int s);
struct client * search_client(char * userid);
struct client * search_device(int device);
void close_client_device(int fd);
void notify_disconnect(int fd);
void close_fd(int fd);
void init_server();
void listen_server();
void * server_command(void * arg);
void close_server();

extern pthread_mutex_t mutex;

#define LOCK pthread_mutex_lock(&mutex);
#define UNLOCK pthread_mutex_unlock(&mutex);

#endif