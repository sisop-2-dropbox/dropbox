#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <libgen.h>
#include "dropboxUtil.h"
#include "dropboxServer.h"

int server_fd = 0;
LISTA * Clients;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/***********************************************************************
 * Sincroniza o servidor com o diretório “sync_dir_<nomeusuário>” do cliente.
 * ********************************************************************/
void sync_server()
{

}


/***********************************************************************
 * Recebe um arquivo file do cliente.
 * Deverá ser executada quando for realizar upload de um arquivo.
 * file - path/filename.ext do arquivo a ser recebido
 * ********************************************************************/
void receive_file(char *file, int fd)
{
	printf("Receiving file...\n");
	char *filename;
	char path[MAXNAME+MAXNAME];
	char *next;
	char filepath[MAXNAME+MAXNAME+MAXNAME+MAXNAME];
	struct client *cli;
	char buffer[BLOCK_SIZE];

	filename = file;
	next = strtok(file, "/");

	while ((next = strtok(NULL, "/"))){
		filename = next;
	}

	LOCK
	cli = search_device(fd);
	UNLOCK

	read(fd, buffer, BLOCK_SIZE);

	if (strcmp(buffer, "rdy") == 0){

		sprintf(buffer, "ack");
		write(fd, buffer, BLOCK_SIZE);

		//printf("ready\n");

		LOCK
		sprintf(path, SERVER_PATH "/%s", cli->userid);
		UNLOCK

		sprintf(filepath, "%s/%s", path, filename);

		struct stat st = {0};

		if (stat(path , &st) == -1){
			mkdir(path, 0700);
		}


		FILE *handler = fopen(filepath, "w");

		//ssize_t r;

		if (handler == NULL){
			sprintf(buffer, "Could not create file.");
			printf("%s\n", buffer);
			write(fd, buffer, BLOCK_SIZE);
		}
		else{
			buffer[0] = ' ';
			while (buffer[0] != '\0'){
				//r = read(fd, buffer, BLOCK_SIZE);
				read(fd, buffer, BLOCK_SIZE);
				fprintf(handler, "%s", buffer);
			}
		}

		fclose(handler);
		printf("File received!...\n");
	}else{
		printf("Error: %s\n", buffer);
	}


}


/***********************************************************************
 * file – path/filename.ext do arquivo a ser recebido
 * Envia o arquivo file para o usuário.
 * Deverá ser executada quando for realizar download de um arquivo.
 * file – filename.ext
 * ********************************************************************/
void send_file(char *file, int fd)
{

}


/***********************************************************************
 * Captura sinal de SIGINT (ctrl+c) enviado ao processo
 * ********************************************************************/
void intHandler(int s)
{
	LOCK
	close_server();
	UNLOCK
	exit(SUCCESS);
}


/***********************************************************************
 * userid - userid do cliente
 * Pesquisa por dado usuário na lista de usuários
 * Retorna NULL caso não encontre
 * ********************************************************************/
struct client * search_client(char * userid)
{
	int i;
	struct client * cli;

	for (i = 0; i < LSIZE(Clients); i++)
	{
		cli = LGET(Clients, i);
		if (cli)
		{
			if (strcmp(cli->userid, userid) == 0)
				return cli;
		}
	}

	return NULL;
}


/***********************************************************************
 * device - dispositivo do cliente. Atualmente eh o fd
 * Pesquisa por dado device na lista de usuários
 * Retorna NULL caso não encontre
 * ********************************************************************/
struct client * search_device(int device)
{
	int i, j;
	struct client * cli;

	for (i = 0; i < LSIZE(Clients); i++)
	{
		cli = LGET(Clients, i);
		if (cli)
		{
			for (j = 0; j < MAXDEVICES; j++)
			{
				if (cli->devices[j] == 0)
					continue;

				if (cli->devices[j] == device)
					return cli;
			}
		}
	}

	return NULL;
}


/***********************************************************************
 * fd - id do socket que identifica o dispositivo conectado
 * Faz o fechamento do dispositivo do cliente identificado por fd
 * Caso o cliente não tenha outros dispositivos conectados,
 * o cliente também será fechado
 * ********************************************************************/
void close_client_device(int fd)
{
	int i, devices;
	struct client * cli, * tmp;

	devices = 0;
	cli = search_device(fd);

	if (cli)
	{
		for (i = 0; i < MAXDEVICES; i++)
		{
			if (cli->devices[i] == fd)
				cli->devices[i] = 0;

			if (cli->devices[i] > 0)
				devices ++;
		}

		if (devices == 0)
		{
			for (i = 0; i < LSIZE(Clients); i++)
			{
				tmp = LGET(Clients, i);
				if (tmp)
				{
					if (strcmp(tmp->userid, cli->userid) == 0)
					{
						free(tmp);
						LDEL(Clients, i);
					}
				}
			}
		}
	}

	close_fd(fd);
}


/***********************************************************************
 * Notifica o cliente que o server está prestes a fechar
 * ********************************************************************/
void notify_disconnect(int fd)
{
	char output[256];

	if (fd)
	{
		bzero(output, 256);
		sprintf(output, "bye");
		write(fd, output, 256);
	}
}


/***********************************************************************
 * Fecha um socket identificado por fd
 * ********************************************************************/
void close_fd(int fd)
{
	if (fd)
	{
		close(fd);
	}
}


/***********************************************************************
 * Inicia o servidor
 * ********************************************************************/
void init_server()
{
	struct sockaddr_in serv_addr;

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		printf("Error opening socket\n");
		exit(ERROR);
	}

	struct stat st = {0};

	if (stat(SERVER_PATH, &st) == -1){
		mkdir(SERVER_PATH, 0700);
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(serv_addr.sin_zero), 8);

	if (bind(server_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		printf("Error on binding\n");
		exit(ERROR);
	}

	Clients = calloc(1, sizeof(LISTA));
	LINIT(Clients);
}


/***********************************************************************
 * Faz o servidor ouvir e o mesmo fica bloqueado aguardando Conexões
 * ********************************************************************/
void listen_server()
{
	socklen_t clilen;
	struct sockaddr_in cli_addr;
	struct client * cli;
	int newfd;
	char buffer[256], output[256], homedir[256];

	clilen = sizeof(struct sockaddr_in);
	listen(server_fd, 5);

	printf("Server listening on port %d\n", PORT);

	while(1)
	{
		if ((newfd = accept(server_fd, (struct sockaddr *) &cli_addr, &clilen)) == -1)
		{
			printf("Error on accept new connection\n");
		}
		else
		{
			bzero(buffer, 256);
			bzero(output, 256);

			if (read(newfd, buffer, 256) < 0)
			{
				printf("Error reading userid from socket\n");
			}
			else
			{
				LOCK
				cli = search_client(buffer);
				UNLOCK

				if (cli == NULL)
				{
					cli = calloc(1, sizeof(struct client));
					cli->devices[0] = newfd;
					cli->logged_in = 1;
					strcpy(cli->userid, buffer);

					bzero(homedir, 256);
					sprintf(homedir, "%s/sync_dir_%s", SERVER_PATH, cli->userid);

					if (open_directory(homedir))
					{
						printf("New client %s\n", cli->userid);

						LOCK
						LAPPEND(Clients, cli);
						UNLOCK

						/* At this point, the client is allowed to connect,
						 we should make a new thread to process commands */
						{
							int * arg;
							pthread_t th;
							arg = calloc(1, sizeof(int));

							if (arg)
							{
								*arg = newfd;
								pthread_create(&th, NULL, server_command, arg);
							}
						}
					}
					else
					{
						sprintf(output, "Failed to open homedir");
						write(newfd, output, 256);
						close_fd(newfd);
					}
				}
				else
				{
					int i, found = 0;

					LOCK
					for (i = 0; i < MAXDEVICES; i++)
					{
						if (cli->devices[i] == 0)
						{
							cli->devices[i] = newfd;
							cli->logged_in = 1;
							found = 1;
							break;
						}
					}
					UNLOCK

					if (found == 0)
					{
						sprintf(output, "No device slot available");
						write(newfd, output, 256);
						close_fd(newfd);
					}
					else
					{
						LOCK
						printf("Existing client %s\n", cli->userid);
						UNLOCK

						/* At this point, the client is allowed to connect,
						 we should make a new thread to process commands */
						{
							int * arg;
							pthread_t th;
							arg = calloc(1, sizeof(int));

							if (arg)
							{
								*arg = newfd;
								pthread_create(&th, NULL, server_command, arg);
							}
						}
					}
				}
			}
		}
	}
}


/***********************************************************************
 * Processa os comandos enviados pelo cliente
 * Esta função sempre vai rodar numa nova thread
 * arg - ponteiro para o fd do cliente
 * ********************************************************************/
void * server_command(void * arg)
{
	int fd;
	struct client * cli;
	char * token;
	char buffer[256];

	if (arg)
	{
		fd = *((int*) arg);

		LOCK
		cli = search_device(fd);
		UNLOCK

		if (cli)
		{
			bzero(buffer, 256);

			LOCK
			sprintf(buffer, "Welcome %s", cli->userid);
			UNLOCK

			if (write(fd, buffer, 256) > 0)
			{
				while(1)
				{
					bzero(buffer, 256);

					if (read(fd, buffer, 256) < 0)
					{
						printf("Error reading from socket on device %d\n", fd);
						break;
					}
					else
					{
						LOCK
						printf("User %s on device %d sent %s\n", cli->userid, fd, buffer);
						UNLOCK

						token = strtok(buffer, " ");

						/**
						 * Upload command
						 **/
						if (strcmp(token, "upload") == 0)
						{
							int size;
							char * buf, * filename;
							char homedir[256];

							token = strtok(NULL, "\n");

							if (token)
							{
								buf = receive_data(fd, &size);

								if (buf)
								{
									filename = basename(token);

									LOCK
									sprintf(homedir, "%s/sync_dir_%s/%s", SERVER_PATH, cli->userid, filename);
									UNLOCK

									write_file(homedir, buf, size);

									LOCK
									printf("User %s on device %d sent the file %s\n", cli->userid, fd, filename);
									UNLOCK

									free(buf);
								}
								else
								{
									printf("Upload command from device %d failed\n", fd);
								}
							}
						}

						/**
						 * Download command
						 **/
						if (strcmp(token, "download") == 0)
						{
							int size;
							char * buf, * filename;
							char homedir[256];

							token = strtok(NULL, "\n");

							if (token)
							{
								filename = basename(token);

								LOCK
								sprintf(homedir, "%s/sync_dir_%s/%s", SERVER_PATH, cli->userid, filename);
								UNLOCK

								buf = read_file(homedir, &size);

								if (buf)
								{
									send_data(fd, buf, size);
									free(buf);

									LOCK
									printf("User %s on device %d downloaded the file %s\n", cli->userid, fd, filename);
									UNLOCK
								}
								else
								{
									LOCK
									printf("Failed to send file %s to User %s on device %d\n", filename, cli->userid, fd);
									UNLOCK

									bzero(buffer, 256);
									sprintf(buffer, "Could not open the file");
									write(fd, buffer, 256);
								}
							}
						}

						/**
						 * List server command
						 **/
						else if (strcmp(token, "list_server") == 0)
						{
							char * buf;
							char homedir[256];

							LOCK
							sprintf(homedir, "%s/sync_dir_%s", SERVER_PATH, cli->userid);
							UNLOCK

							buf = list_directory(homedir);

							if (buf)
							{
								send_data(fd, buf, strlen(buf));
								free(buf);
							}
						}

						/**
						 * Get file list command
						 **/
						else if (strcmp(token, "get_file_list") == 0)
						{
							char * buf;
							char homedir[256];

							LOCK
							sprintf(homedir, "%s/sync_dir_%s", SERVER_PATH, cli->userid);
							UNLOCK

							buf = get_file_list(homedir);

							if (buf)
							{
								if (strlen(buf) == 0)
								{
									send_data(fd, "error", 5);
								}
								else
								{
									send_data(fd, buf, strlen(buf));
								}
								free(buf);
							}
							else
							{
								send_data(fd, "error", 5);
							}
						}

						/**
						 * Get sync dir command
						 **/
						else if (strcmp(token, "get_sync_dir") == 0)
						{
						}

						/**
						 * Exit command
						 **/
						else if (strcmp(token, "exit") == 0)
						{
							break;
						}

						/**
						 * Modification time request command
						 **/
						else if (strcmp(token, "request_mtime") == 0)
						{
							char homedir[256];
							time_t t;

							bzero(buffer, 256);

							if (read(fd, buffer, 256) > 0)
							{
								LOCK
								sprintf(homedir, "%s/sync_dir_%s/%s", SERVER_PATH, cli->userid, basename(buffer));
								UNLOCK

								t = get_mtime(homedir);
								bzero(buffer, 256);

								if (t)
								{
									sprintf(buffer, "%ld", t);
								}
								else
								{
									sprintf(buffer, "error");
								}

								write(fd, buffer, 256);
							}
						}

						/**
						 * Modification time change command
						 **/
						else if (strcmp(token, "change_mtime") == 0)
						{
							char homedir[256];
							time_t t;

							bzero(buffer, 256);

							if (read(fd, buffer, 256) > 0)
							{
								LOCK
								sprintf(homedir, "%s/sync_dir_%s/%s", SERVER_PATH, cli->userid, basename(buffer));
								UNLOCK

								bzero(buffer, 256);

								if (read(fd, buffer, 256) > 0)
								{
									t = atoi(buffer);
									bzero(buffer, 256);

									if (set_mtime(homedir, t))
									{
										sprintf(buffer, "done");
									}
									else
									{
										sprintf(buffer, "error");
									}

									write(fd, buffer, 256);
								}
							}
						}

						/**
						 * Delete file request
						 **/
 						else if (strcmp(token, "delete_file") == 0)
 						{
							char homedir[256];

							token = strtok(NULL, "\n");

 							if (token)
 							{
 								// TODO: check sanity

								LOCK
								sprintf(homedir, "%s/sync_dir_%s/%s", SERVER_PATH, cli->userid, basename(token));
								UNLOCK

								bzero(buffer, 256);

								if (delete_file(homedir))
								{
									sprintf(buffer, "done");
								}
								else
								{
									sprintf(buffer, "error");
								}

								write(fd, buffer, 256);
 							}
 						}

					}
				}
			}
		}

		LOCK
		close_client_device(fd);
		UNLOCK

		free(arg);
	}

	return 0;
}


/***********************************************************************
 * Faz o fechamento do server e todos os clientes conectados
 * ********************************************************************/
void close_server()
{
	int i, j;
	struct client * cli;

	for (i = 0; i < LSIZE(Clients); i++)
	{
		cli = LGET(Clients, i);
		if (cli)
		{
			for (j = 0; j < MAXDEVICES; j++)
			{
				if (cli->devices[j] != 0)
				{
					notify_disconnect(cli->devices[j]);
					close_fd(cli->devices[j]);
				}
			}
			free(cli);
		}
		LDEL(Clients, i);
	}

	if (server_fd)
	{
		close(server_fd);
		server_fd = 0;
	}
}


/***********************************************************************
 * Função 'main()' do Servidor
 * ********************************************************************/
int main(int argc, char *argv[])
{
	/* SIGINT handler (ctrl+c) */
	struct sigaction act_sigint;
	act_sigint.sa_handler = intHandler;
	sigaction(SIGINT, &act_sigint, NULL);

	/* SIGTERM handler */
	struct sigaction act_sigterm;
	act_sigterm.sa_handler = intHandler;
	sigaction(SIGTERM, &act_sigterm, NULL);

	init_server();
	listen_server();

	LOCK
	close_server();
	UNLOCK

	return SUCCESS;
}
